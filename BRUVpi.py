# El programa dispara 100 secuencias de 5 minutos (300s) cubriendo 8,33333333334 horas de grabación.

import time
from time import sleep
from picamera import PiCamera
from datetime import datetime, timedelta

camera = PiCamera()
camera.resolution = (1920, 1080)
for filename in camera.record_sequence(
        '%y%b%d_%H:%M:%S.h264' % i for i in range(1, 100)):   # Cambia el valor 100 para variar el numero de secuencias capturadas
    camera.wait_recording(300)                                # Cambia el valor 300 (segundos) para variar la duración de cada secuencia
