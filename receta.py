import picamera

camera = picamera.PiCamera(resolution=(1920, 1080))
for filename in camera.record_sequence(
        '/home/pi/media/%d.h264' % i for i in range(1, 100)):
    camera.wait_recording(5)

